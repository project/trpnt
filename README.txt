
-- SUMMARY --

The Taxonomy Required per Node Type module lets you set which taxonomies are
required per node type.

For a full description of the module, visit the project page:
  http://drupal.org/project/trpnt

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/trpnt


-- REQUIREMENTS --

Taxonomy


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Users with the 'administer taxonomy' permission can go to Administer >>
  Site configuration >> Taxonomy Required per Node Type <admin/settings/trpnt>
  to set Taxonomy required setting per node type.


-- TROUBLESHOOTING --

Empty.


-- FAQ --

Q: Empty

A: Empty.


-- CONTACT --

Maintainer:
* Rodrigo Severo (rsevero) - http://drupal.org/user/496564

This project is sponsored by:
* Fábrica de Idéias
  Drupal based site development in Brazil. Visit http://www.fabricadeideias.com
  for more information.
